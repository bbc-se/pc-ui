/**
 * 众筹活动相关API
 */

import request from '@/utils/request'

/**
 * 分页列表
 * @param params
 */
export function getList(params) {
  return request({
    url: '/buyer/promotions/crowdfunding',
    method: 'get',
    params
  })
}

/**
 * 查询详情
 * @param id
 */
export function getDetail(id) {
  return request({
    url: `/buyer/promotions/crowdfunding/${id}`,
    method: 'get'
  })
}

/**
 * 立即购买
 * @param crowdfunding_id
 * @param num
 */
export function buy(crowdfunding_id, num) {
  return request({
    url: `/buyer/crowdfunding/buy`,
    method: 'get',
    params: { crowdfunding_id, num },
    needToken: true
  })
}

/**
 * 获取购物车
 */
export function getCart() {
  return request({
    url: `/buyer/crowdfunding/cart`,
    method: 'get',
    needToken: true
  })
}

/**
 * 提交订单
 */
export function createTrade() {
  return request({
    url: `/buyer/crowdfunding/trade`,
    method: 'post',
    needToken: true,
    params: { client: 'PC' }
  })
}
