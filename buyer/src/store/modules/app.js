import * as types from '../mutation-types'
import * as API_Common from '../../api/common'
import * as API_Home from '../../api/home'
import * as API_Message from '../../api/message'
import uuid from 'uuid/v1'
import Storage from '../../utils/storage'

if (!Storage.getItem('uuid')) {
  Storage.setItem('uuid', uuid(), { expires: 30 })
}

/** state */
export const state = () => ({
  // 导航栏
  navList: [],
  // 分类
  categories: [],
  // 热搜关键词
  hotKeywords: [],
  // 站点信息
  site: '',
  // 未读消息数量
  unreadMessageNum: 0,
  // 环境变量
  env: '',
  // referer
  referer: ''
})

/** mutations */
export const mutations = {
  /**
   * 设置站点cookie
   * @param state
   * @param data
   */
  [types.SET_SITE_DATA](state, data) {
    state.site = data
    Storage.setItem('site', global.JSON.stringify(data))
  },
  /**
   * 设置分类数据
   * @param state
   * @param data
   */
  [types.SET_CATEGORY_DATA](state, data) {
    state.categories = data
  },
  /**
   * 设置导航栏数据
   * @param state
   * @param data
   */
  [types.SET_NAV_DATA](state, data) {
    state.navList = data
  },
  /**
   * 设置热搜关键词数据
   * @param state
   * @param data
   */
  [types.SET_HOT_KEYWORDS](state, data) {
    state.hotKeywords = data
  },
  /**
   * 未读消息数量
   * @param state
   * @param num
   */
  [types.GET_UNREAD_MESSAGE](state, num) {
    state.unreadMessageNum = num
  },
  /**
   * 设置环境变量
   * @param state
   * @param env
   */
  [types.SET_ENV_VARS](state, env) {
    state.env = env
  },
  [types.REFERER](state, env) {
    state.referer = env
  }
}

/** actions */
export const actions = {
  /**
   * 获取公共数据
   * @param commit
   * @returns {Promise<void>}
   */
  async getCommonDataAction({ commit }) {
    // 站点信息
    API_Common.getSiteData().then(res => {
      commit(types.SET_SITE_DATA, res)
    })
    // 导航栏
    API_Home.getSiteMenu().then(res => {
      commit(types.SET_NAV_DATA, res)
    })
    // 分类数据
    API_Home.getCategory().then(res => {
      commit(types.SET_CATEGORY_DATA, res)
    })
    // 热门关键字
    API_Home.getHotKeywords().then(res => {
      commit(types.SET_HOT_KEYWORDS, res)
    })
  },
  /**
   * 获取未读消息数
   * @param commit
   * @returns {Promise<void>}
   */
  async getUnreadMessageNumAction({ commit }) {
    const res = await API_Message.getNoReadMessageNum()
    commit(types.GET_UNREAD_MESSAGE, res.total)
  }
}

export const getters = {
  /**
   * 分类列表
   * @param state
   * @returns {*}
   */
  categories: state => state.categories,
  /**
   * 导航栏
   * @param state
   * @returns {*}
   */
  navList: state => state.navList,
  /**
   * 热搜关键词
   * @param state
   * @returns {*}
   */
  hotKeywords: state => state.hotKeywords,
  /**
   * 获取站点信息
   * @param state
   * @returns {getters.site|(function(*))|string}
   */
  site: state => state.site,
  /**
   * 获取未读消息数量
   * @param state
   * @returns {getters.unreadMessageNum|(function(*))|*|number}
   */
  unreadMessageNum: state => state.unreadMessageNum,
  /**
   * 获取环境变量
   * @param state
   * @returns {*}
   */
  env: state => state.env
}
