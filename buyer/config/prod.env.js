const { im, distribution } = require('./index')

module.exports = {
	NODE_ENV: '"production"',
	ENV_CONFIG: '"prod"',
  IM: im,
	DISTRIBUTION: distribution
}
