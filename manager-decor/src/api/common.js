/**
 * 公共API
 */
import request from '@/utils/request'
import Storage from '@/utils/storage'
import { decorType } from '@/utils/decor-types'

/**
 * seller/check/token
 * seller_refresh_token
 * 刷新token
 */
export function refreshToken() {
  return request({
    url: decorType === 'seller' ? 'seller/check/token' : 'admin/systems/admin-users/token',
    method: 'post',
    data: {
      refresh_token: Storage.getItem(`${decorType}_refresh_token`)
    }
  })
}
