# shopTNT电商系统-前端（PC端 商家PC端 管理端）

#### 介绍
ShopTNT电商系统是一个全面的电子商务解决方案，旨在为用户提供一个高效、灵活且用户友好的在线购物平台。该系统通常分为三个主要的前端组件：消费者PC端、商家PC端和管理端

### 演示地址

平台管理端：https://manager-bbc.shoptnt.cn 账号：admin/111111

店铺管理端：https://seller-bbc.shoptnt.cn 账号：ceshi/111111

商城PC页面：https://shop-bbc.shoptnt.cn 账号可自行注册

商城移动端，扫描如下二维码

![](https://shoptnt-bbc.oss-cn-beijing.aliyuncs.com/tnt/weichat-gongzhonghao.jpg?x-oss-process=style/300x300)

### 开发/部署文档中心

https://docs.shoptnt.cn/docs/5.2.3

## 项目地址

后台API：https://gitee.com/bbc-se/api

前端ui：https://gitee.com/bbc-se/pc-ui

移动端：https://gitee.com/bbc-se/mobile-ui

配置中心：https://gitee.com/bbc-se/config

## 使用须知

1. 允许个人学习使用。
2. 允许用于学习、毕业设计等。
3. 禁止将本开源的代码和资源进行任何形式任何名义的出售。
4. 限制商业使用，如需[商业使用](http://www.shoptnt.cn)联系QQ：2025555598 或微信扫一扫加我微信。

![](https://shoptnt-bbc.oss-cn-beijing.aliyuncs.com/tnt/danren.png?x-oss-process=style/300x300)

## 交流、反馈

### 推荐
官方微信群：

![](https://shoptnt-statics.oss-cn-beijing.aliyuncs.com/qun.png?x-oss-process=style/300x300)