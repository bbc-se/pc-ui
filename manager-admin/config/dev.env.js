const { distribution, liveVideo, o2o, supplier, im } = require('./index')

module.exports = {
	NODE_ENV: '"development"',
	ENV_CONFIG: '"dev"',
  DISTRIBUTION: distribution,
  LIVEVIDEO: liveVideo,
  O2O: o2o,
  SUPPLIER: supplier,
  IM: im
}
