const getters = {
  sidebar: state => state.app.sidebar,
  language: state => state.app.language,
  device: state => state.app.device,
  visitedViews: state => state.tagsView.visitedViews,
  cachedViews: state => state.tagsView.cachedViews,
  user: state => state.user.user,
  permission_routers: state => state.permission.routers,
  addRouters: state => state.permission.addRouters,
  siteInfo: state => state.app.siteInfo,
  permission_chat: state => state.permission.permission_chat
}
export default getters
