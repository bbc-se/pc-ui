/**
 * 系统设置
 */

import request from '@/utils/request'
import md5 from 'js-md5'

/**
 * 获取站点设置
 */
export function getSiteSetting() {
  return request({
    url: 'admin/settings/site',
    method: 'get',
    loading: false
  })
}

/**
 * 修改站点设置
 * @param params
 */
export function editSiteSetting(params) {
  return request({
    url: 'admin/settings/site',
    method: 'put',
    data: params
  })
}

/**
 * 获取积分设置
 */
export function getPointSetting() {
  return request({
    url: 'admin/settings/point',
    method: 'get'
  })
}

/**
 * 修改积分设置
 * @param params
 */
export function editPointSetting(params) {
  return request({
    url: 'admin/settings/point',
    method: 'put',
    data: params
  })
}

/**
 * 获取账号安全设置
 */
export function getAccountSetting() {
  return request({
    url: 'admin/settings/account',
    method: 'get'
  })
}

/**
 * 修改账号安全设置
 * @param params
 */
export function editAccountSetting(params) {
  return request({
    url: 'admin/settings/account',
    method: 'put',
    data: params
  })
}

/**
 * 查询二次身份验证设置
 */
export function getAuthenticationsSetting() {
  return request({
    url: 'admin/systems/authentications',
    method: 'get'
  })
}

/**
 * 修改二次身份验证设置
 * @param params
 */
export function editAuthenticationsSetting(params) {
  console.log(params)
  if (params.password) {
    params.password = md5(params.password)
  }
  return request({
    url: 'admin/systems/authentications',
    method: 'put',
    data: params
  })
}

/**
 * 重置促销活动
 * @param params
 */
export function redisPromotion() {
  return request({
    url: 'admin/promotion/script-init',
    method: 'post'
  })
}
