/**
 * 众筹活动相关API
 */

import request from '@/utils/request'

/**
 * 分页列表
 * @param params
 */
export function getList(params) {
  return request({
    url: '/admin/promotion/crowdfunding',
    method: 'get',
    params
  })
}

/**
 * 添加
 * @param params
 */
export function add(params) {
  return request({
    url: '/admin/promotion/crowdfunding',
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    data: params
  })
}

/**
 * 查询详情
 * @param id
 */
export function getDetail(id) {
  return request({
    url: `/admin/promotion/crowdfunding/${id}`,
    method: 'get'
  })
}

/**
 * 修改企业
 * @param id
 * @param params
 */
export function edit(id, params) {
  return request({
    url: `/admin/promotion/crowdfunding/${id}`,
    method: 'put',
    headers: { 'Content-Type': 'application/json' },
    data: params
  })
}

/**
 * 删除
 * @param id
 */
export function del(id) {
  return request({
    url: `/admin/promotion/crowdfunding/${id}`,
    method: 'delete'
  })
}

/**
 * 终止
 * @param id
 */
export function stop(id) {
  return request({
    url: `/admin/promotion/crowdfunding/${id}/stop`,
    method: 'put'
  })
}

/**
 * 终止
 * @param id
 * @param flag
 */
export function updateEndShowFlag(id, flag) {
  return request({
    url: `/admin/promotion/crowdfunding/${id}/updateEndShowFlag?flag=${flag}`,
    method: 'put'
  })
}

